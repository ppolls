create table if not exists entries (
id integer primary key autoincrement,
type string not null,
title string not null,
text string not null,
uid integer not null,
hits integer not null);
insert into entries values (1,'radio','first question','Is this the first question?',1,0);
insert into entries values (2,'checkbox','second question','Is this not the first question?',1,0);

create table if not exists answers (id integer primary key autoincrement,qid integer,content string not null,hits integer default 0);
insert into answers values (1,1,'yes',0);
insert into answers values (2,1,'no',0);
insert into answers values (3,1,'do not know',0);
insert into answers values (4,2,'yes!',0);
insert into answers values (5,2,'no!',0);
insert into answers values (6,2,'do not know...',0);

    create table if not exists users (
    id integer primary key autoincrement,
    username string not null,
    password string not null,
    type integer not null,
    hit string,
    email string,
    fullname string
  );
  
  insert into users values (0,'a','c',0,"","e@ma.il","a full name");
