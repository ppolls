package Ppolls;
use Dancer ':syntax';
#use DBI;
use File::Spec;
use File::Slurp;
use Template;
use Dancer::Plugin::Database;
use Crypt::SaltedHash;
use Data::Dumper;
use Dancer::Plugin::FlashMessage;

our $VERSION = '0.2';

set 'session' => 'Simple';
set 'template' => 'template_toolkit';
set 'logger' => 'console';
set 'log' => 'debug';
set 'show_errors' => 1;
set 'access_log' => 1;
set 'warnings' => 1;

# layout 'main';
set layout => "main";

before_template sub {
	my $tokens = shift;
	
	$tokens->{'css_url'} = request->base . 'css/style.css';
	$tokens->{'login_url'} = uri_for('/login');
	$tokens->{'logout_url'} = uri_for('/logout');
	$tokens->{'showall_url'} = uri_for('/showall');
	$tokens->{'vote_url'} = uri_for('/vote');
	$tokens->{'register_url'} = uri_for('/register');
	$tokens->{'add_url'} = uri_for('/add');
	$tokens->{'changepass_url'} = uri_for('/changepass');
	$tokens->{'showpopulardesc_url'} = uri_for('/showpopular-desc');
	$tokens->{'showpopularasc_url'} = uri_for('/showpopular-asc');
};

before sub {

   if (! session('user') && 
         request->path_info !~ m{^/(login|register|showall|showpopular.*)}
   ) {
         var requested_path => request->path_info;
         request->path_info('/login');
   }
};

get '/login' => sub {
   template 'login', { path => vars->{requested_path} };
};

post '/login' => sub {  
    my $user = database('users')->prepare(
        'select * from users where username = ?',
    );
    $user->execute(params->{username});
    my $userdata = $user->fetchrow_hashref();
    if (!$userdata) {
        flash error => "Failed login for unrecognised user " 
                       . params->{username}.'.';
        redirect uri_for('/login');
    } else {
        if ($userdata->{password} eq params->{password})
        {
            session user => $user;
            session uid => $userdata->{'id'};
            session username => $userdata->{'username'};
            redirect params->{path} || uri_for('/showall');
        } else {
            flash error => 'Login failed - password incorrect for <span class="username">' . params->{username}.'</span>.';
            redirect uri_for('/login');
        }
    }
};

get '/logout' => sub {
    session->destroy;
    flash error => 'You are logged out.';
    redirect uri_for('/showall');
};

get '/register' => sub {
   template 'register', { path => params->{'path'} };
};

post '/register' => sub {      
    if (params->{'username'} =~ m/\W/){
        flash error => 'Please use only [A-Za-z0-9] in the username. Thank you.';
        redirect uri_for('/register');
        return;
    }
    if (params->{'username'} eq ''){
        flash error => 'Please set a username when registering. Thank you.';
        redirect uri_for('/register');
        return;
    }
    if (params->{'email'} eq ''){
        flash error => 'Please set an email when registering. This will be used for password recoveries only. Thank you.';
        redirect uri_for('/register');
        return;
    }

    # Check if username is taken.
    my $username = database('users')->selectall_arrayref(
      'select * from users where username = ?',{},params->{'username'});
    if (@$username){
        flash error => 'Error: username in already in use.';
        redirect uri_for('/register');
        return;
    } else {
        my $user = database('users')->do(
          "insert into users values(NULL, ?, ?, ?, ?, ?, ?)",{},
            (params->{'username'},
            params->{'password'},
            "user",
            0,
            params->{'email'},
            params->{'fullname'})         
        );
        flash error => 'Registration of user <span class="username">'.
          params->{username}.'</span> successful.';
        template 'login', { username => (params->{'username'}), 
                            password => (params->{'password'}), 
                            path => (params->{'path'})
                          };
    }
};

get '/show/:id' => sub {
    my $hash_ref = database('questions')->selectrow_hashref
      ('select id, title, text, type, hits from entries where id = ?', {}, params->{id});
    my $hash_ref2 = database('questions')->selectall_hashref
      ('select id,content,qid,hits from answers where qid = ?','id',{},params->{id});
    my $user_hit = database('users')->selectrow_hashref
      ('select hit from users where id = ?', {}, session('uid'));
    my $hit = $user_hit->{'hit'};
    my $id = scalar(params->{'id'});
    my $matches = $hit =~ /$id/; # voted yet ?
    template 'show_entry', { 
            id        => params->{id},
            title     => $hash_ref->{'title'},
            type      => $hash_ref->{'type'},
            text      => $hash_ref->{'text'},
            hits      => $hash_ref->{'hits'},
            answers   => $hash_ref2,
            voted     => $matches,
            qid       => params->{id}
    };
};

get '/showall' => sub {
    my $sql = 'select id, title, text from entries order by id desc';
    my $sth = database('questions')->prepare($sql) 
      or die database('questions')->errstr;
    $sth->execute or die $sth->errstr;
    template 'show', { 
            'add_entry_url' => uri_for('/add'),
            'entries' => $sth->fetchall_hashref('id'),
    };
};

get '/showpopular-desc' => sub {
    my $hash = database('questions')->selectall_arrayref
      ('select id, title, text, hits from entries order by hits desc');
    template 'show-desc', { 
            'entries' => $hash
    }; 
};


get '/showpopular-asc' => sub {
    my $hash = database('questions')->selectall_arrayref
      ('select id, title, text, hits from entries order by hits asc');
    template 'show-asc', { 
            'entries' => $hash
    }; 
};

post '/vote' => sub {
    my $answer_type = ref(\(params->{'value'}));
    if ($answer_type eq 'REF') {
        my @values = @{params->{'value'}};
        my $value;
        foreach $value (@values){
          database('questions')->do
            ("UPDATE answers SET hits = hits + 1 WHERE id = ?",{},$value);
        }
    }
    if ($answer_type eq 'SCALAR'){
        database('questions')->do
          ("UPDATE answers SET hits = hits + 1 WHERE id = ?",{},params->{'value'});
    }
    # User hit this question.
    database('questions')->do
      ("UPDATE entries SET hits = hits + 1 WHERE id = ?",{},params->{'qid'});
      # add more hit
    database('users')->do
      ("UPDATE users SET hit = hit || ' ' || ? WHERE id = ?",{},(
        params->{'qid'},
        session('uid')
      ));
    redirect uri_for('/show/'.params->{'qid'});
};

get '/add' => sub {
   template 'add';
};

post '/add' => sub {  
    if (params->{'title'} eq ""){
        flash error => 'Error: empty title.';
        redirect uri_for('/add');
    }
    if (params->{'content'} eq ''){
        flash error => 'Error: empty content.';
        redirect uri_for('/add');
    }
    if (params->{'answers'} eq ''){
        flash error => 'Error: no answers given.';
        redirect uri_for('/add');
    }
    database('questions')->do
      ("insert into entries values(NULL, ?, ?, ?, ?, 0)",{},(
        params->{'type'},
        params->{'title'},
        params->{'content'},
        session('uid'),      
      ));
    # TODO: get the id.
    my $qid = database('questions')->last_insert_id("",database('questions'),'entries','id');
    #my $qid = (database('questions')->selectrow_hashref
    #  ("select id from entries where text = ?",{},params->{'content'}))->{'id'};
    my $answer_choice;
    foreach $answer_choice (split /\n/, params->{'answers'}){
        $answer_choice =~ s/\n//;
        $answer_choice =~ s/\r//;
        database('questions')->do
          ("insert into answers values(NULL,?, ?, 0)",{},(
            $qid,
            $answer_choice
          ));
    }
    redirect uri_for("show/$qid");
};
get '/changepass' => sub {
   template 'changepass';
};

post '/changepass' => sub {
    if (!(params->{"newpassword"} eq params->{"newpassword2"})){
        flash error => "New passwords mismatch.";
        redirect uri_for('/changepass');
    }
    else{
        database('users')->do
          ("update users set password = ? where id = ?",{},
            (params->{'newpassword'},session('uid'))
          );
        flash error => "Password updated successfully.";
        redirect uri_for('/changepass');
    }

};

true;
